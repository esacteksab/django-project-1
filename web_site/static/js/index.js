;(function(){
	
	/**
	* Client-side index.
	* We will load all other client-side resources through this file. 
	* Including bootstrap and jquery.
	*/

	var easyScroll = function easyScroll(offset, cb) {

		$('html, body').stop(true).animate({ scrollTop : ( (offset - 75 ) < 0 ? 0 : (offset - 75) ) }, {
			duration : 300
		}, function(){
			
			if((cb) && (typeof cb == "function")) {

				cb();
			}
		});
	};

	var processForm = function processForm() {

		var data = {};

		$('#contact-form :input').each(function() {

			if($(this).attr('name')) {

				data[$(this).attr('name')] = $(this).val() || undefined;
			}
		});

		var err = badForm(data);

		if(!err) { return submitForm(data);  }

		formError(err);
	};

	var badForm = function badForm(data) {

		if(!data.name) { return "name"; }
		if(!data.email) { return "email"; }
		if(data.email.split('@').length != 2) { return "email"; }
		if(!data.message) { return "message"; }
		return false;
	};

	// try to submit the form without burning the house down
	var submitForm = function submitForm(data) {

		$.ajax({

			type : "POST"
			, url : "/contact"
			, data : data
			, dataType : "json"
			, success: function(err, dat) {

				if(!err) {

					return formSuccess();
				}

				formFailed();
			}
		});
	};

	// There was a problem with form validation
	var formError = function thisIsWhyWeCantHaveNiceThings(err) {

		flipTip(_cs(err));
	};

	// There was no error and things are fantastic
	var formSuccess = function everythingWentBetterThanExpected() {

		flipTip('#contact h1');
		$('#contact-form :input').val('');
	};

	var formFailed = function burnTheHouseDown() {

		flipTip('#contact h1');
	};

	var flipTip = function lolGetItFlipTip(tip) {

		$(tip).tooltip('show');

		setTimeout(function() {

			$(tip).tooltip('hide');
		}, 5000);
	};

	var _cs = function contactFormElementSelector(sel) {

		return [ 
			'#contact-form input[name="'
			, sel
			, '"]'
		].join('');
	};

	var tips = function createTooltipsForContactForm() {

		$('#contact-form :input').each(function() {

			if($(this).attr('name')) {

				$(this).tooltip({

					"title" : "Please provide your " + $(this).attr('name')
					, "placement" : "left"
					, "trigger" : "manual"
				});
			}
		});

		$('#contact > div > h1').tooltip({

			"title" : "Thank you for contacting us!"
			, "placement" : "right"
			, "trigger" : "manual"
		});
	};

	$(function() {

		tips();

		$('a').click(function(e) {

			var href = $(this).attr('href') || undefined;
			if((href.substr(0, 1) == '#') && ($(href))) {

				e.preventDefault();
				easyScroll($(href).offset().top);
			}
		});

		$('#contact-form button').click(function(e) {

			e.preventDefault();
			// This is the submit button
			if($(this).hasClass('btn-primary')) {

				processForm();
			}
			// This is the clear button
			else {

				$('#contact-form :input').val('');
			}

			// yes, this is necessary.
		});

		$('#carousel').carousel({

  			interval : 5000
		});

	});

})();