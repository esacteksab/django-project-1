$("form.contact").live("submit", function(e) {
    e.preventDefault();
    var $form = $(this);
    $.ajax({
        url: $form.attr("action"),
        type: $form.attr("method"),
        data: $form.serialize(),
        statusCode: {
            200: function(data) {
                if (data.status == "error") {
                    // This if/else is only needed in case you wanted
                    // to do something else client side based on
                    // detection of error or not, otherwise, you can
                    // just always replaceWith the html contents that
                    // comes back from the server which will have the
                    // form validation bits
                    $form.replaceWith($(data.html));
                } else {
                    $form.replaceWith($(data.html));
                }
            }
        }
    })
});