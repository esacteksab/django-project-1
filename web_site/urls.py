from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import DeleteView, UpdateView, ListView, CreateView
from web_site.apps.artists.models import VisitingArtist
from web_site.apps.sponsors.models import Sponsor
from web_site.apps.artists.forms import ArtistForm
from web_site.apps.sponsors.forms import SponsorForm
from web_site.apps.press.models import Article, Page
from web_site.apps.press.forms import ArticleForm, PageForm

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'web_site.apps.info.views.Contact', name='home'),

    # url(r'^artists/$', 'web_site.apps.artists.views.displayArtists'),
    # url(r'^sponsors/$', 'web_site.apps.sponsors.views.displaySponsors'),
    # url(r'^articles/$', 'web_site.apps.press.views.publications'),
    (r'^robots.txt$', include('robots.urls')),

    url(r'^adm/artists/$', 'web_site.apps.adm.views.displayArtists'),
    url(r'^adm/artists/add/$', 'web_site.apps.artists.views.add_artist'),
    url(r'^adm/artists/delete/(?P<pk>\d+)/$', DeleteView.as_view(
        model=VisitingArtist,
        success_url=('/adm/artists/'))),
    url(r'^adm/artists/edit/(?P<pk>\d+)/$', UpdateView.as_view(
        model=VisitingArtist,
        form_class=ArtistForm,
        success_url=('/adm/artists'),
        template_name="adm-artist.html")),

    url(r'^adm/sponsors/$', 'web_site.apps.adm.views.displaySponsors'),
    url(r'^adm/sponsors/add/$', 'web_site.apps.sponsors.views.add_sponsor'),
    url(r'^adm/sponsors/delete/(?P<pk>\d+)/$', DeleteView.as_view(
        model=Sponsor,
        success_url=('/adm/sponsors'))),
    url(r'^adm/sponsors/edit/(?P<pk>\d+)/$', UpdateView.as_view(
        model=Sponsor,
        form_class=SponsorForm,
        success_url=('/adm/sponsors'),
        template_name="adm-sponsor.html")),

    url(r'^adm/article/$', ListView.as_view(
        queryset=Article.objects.all().order_by('-year'),
        template_name="adm-articles.html")),
    url(r'^adm/article/add/$', 'web_site.apps.press.views.add_article'),
    url(r'^adm/article/delete/(?P<pk>\d+)/$', DeleteView.as_view(
        model=Article,
        success_url=('/adm/article'))),
    url(r'^adm/article/edit/(?P<pk>\d+)/$', UpdateView.as_view(
        model=Article,
        form_class=ArticleForm,
        success_url=('/adm/article'),
        template_name="adm-article.html")),

    url(r'^adm/page/$', ListView.as_view(
        model=Page,
        template_name="adm-pages.html")),
    url(r'adm/page/add/$', CreateView.as_view(
        model=Page,
        form_class=PageForm,
        success_url=('/adm/page'),
        template_name="adm-page.html"),
        name="createPage"),
    url(r'^adm/page/edit/(?P<pk>\d+)/$', UpdateView.as_view(
        model=Page,
        form_class=PageForm,
        success_url=('/adm/page'),
        template_name="adm-page.html")),
    url(r'^adm/page/delete/(?P<pk>\d+)/$', DeleteView.as_view(
        model=Page,
        success_url=('/adm/page'))),

    url(r'^adm/$', 'web_site.apps.adm.views.home'),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout'),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
