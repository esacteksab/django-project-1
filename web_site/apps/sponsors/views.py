from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
from web_site.apps.sponsors.models import Sponsor
from web_site.apps.sponsors.forms import SponsorForm


@login_required(login_url='/accounts/login')
def add_sponsor(request):
    form = SponsorForm()
    if request.method == 'POST':
        form = SponsorForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/adm/sponsors')
    template = 'adm-sponsor.html'
    data = {
        'form': form,
    }

    return render_to_response(template, data, context_instance=RequestContext(request))


@login_required(login_url='/accounts/login/')
def delete_sponsor(self, pk=None):
    Sponsor.objects.get(pk=pk).delete()

    return HttpResponseRedirect('/adm/sponsors')


@login_required(login_url='/accounts/login/')
def edit_sponsor(self, pk=None):
    Sponsor.objects.get(pk=pk)
    return HttpResponseRedirect('/adm/sponsors')


def displaySponsors(request):
    """Display all Aritsts"""
    sponsors = Sponsor.objects.all()
    return render_to_response('sponsors.html',
        {'sponsors': sponsors},
            context_instance=RequestContext(request))
