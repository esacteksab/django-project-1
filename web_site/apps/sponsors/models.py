from django.db import models
from django.utils.translation import ugettext_lazy as _


class Sponsor(models.Model):
    name = models.CharField(_("name"), max_length=50, null=True, blank=True)
    bio = models.CharField(_("bio"), max_length=255, null=True, blank=True)
    logo = models.ImageField(_("logo"), upload_to="images/", blank=True, null=True)
    website = models.URLField(_("website"), null=True, blank=True, verify_exists=False)

    def __unicode__(self):
        return '%s' '%s' '%s' '%s' % (self.name, self.bio, self.website, self.logo)