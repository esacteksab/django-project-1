from django.contrib import admin
from web_site.apps.sponsors.models import Sponsor


class SAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,    {'fields': ['name']}),
        (None,    {'fields': ['bio']}),
        (None, {'fields': ['website']}),
        (None, {'fields': ['logo']})
    ]
    list_display = ('name', 'bio', 'website', 'logo')

admin.site.register(Sponsor, SAdmin)
