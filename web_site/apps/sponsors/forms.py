from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
from crispy_forms.bootstrap import *
from web_site.apps.sponsors.models import Sponsor


class SponsorForm(ModelForm):
    """A form to allow the creation of a new sponsor"""
    class Meta:
        model = Sponsor
        fields = ['name', 'bio', 'website', 'logo']

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_id = 'add-sponsor'
        self.helper.form_class = 'form-inline'
        self.helper.form_action = ''
        self.helper.layout = Layout(
        Fieldset("Add a new Sponsor",
            Field('name', css_class="input-large", placeholder="Sponsor Name"),
            Field('bio', css_class="input-large", placeholder="Sponsor Bio"),
            Field('website', css_class="input-large", placeholder="Sponsor Website"),
            Field('logo'),
        ),
        FormActions(
            Submit('submit', "Submit", css_class='btn btn-large'),
        )
    )
        super(SponsorForm, self).__init__(*args, **kwargs)
