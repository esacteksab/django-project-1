from django.contrib import admin
from web_site.apps.press.models import Article, Page


class PageAdmin(admin.ModelAdmin):
    list_display = ["__unicode__", "article", "page", "pgnumber"]

    def save_model(self, request, obj, form, change):
        obj.save()

admin.site.register(Page, PageAdmin)
admin.site.register(Article)
