from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
from web_site.apps.press.forms import ArticleForm, PageForm
from web_site.apps.press.models import Page, Article


def publications(request):
    articles = Article.objects.all().order_by('-year')
    pages = Page.objects.filter(article__in=articles).order_by('pgnumber')

    return render_to_response('articles.html',
        {'articles': articles, 'pages': pages},
        context_instance=RequestContext(request))


@login_required(login_url='/accounts/login')
def add_article(request):
    """Add a new Visiting Artist"""
    form = ArticleForm()
    if request.method == 'POST':
        submit = request.POST.get('cancel', None)
        if submit:
            return HttpResponseRedirect('/adm/article')
        else:
            form = ArticleForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect('/adm/article')
    template = 'adm-article.html'
    data = {
        'form': form,
    }

    return render_to_response(template, data, context_instance=RequestContext(request))


@login_required(login_url='/accounts/login/')
def delete_article(self, pk=None):
    """Delete an Artists"""
    Article.objects.get(pk=pk).delete()

    return HttpResponseRedirect('/adm/article')


@login_required(login_url='/accounts/login/')
def edit_article(self, request, pk=None):
    """Edit An Artist"""
    if request.method == 'POST':
        submit = request.POST.get('cancel', None)
        if submit:
            return HttpResponseRedirect('/adm/article')
        else:
            Article.objects.get(pk=pk)
    return HttpResponseRedirect('/adm/article')


@login_required(login_url='/accounts/login')
def add_page(request):
    """Add a new Visiting Artist"""
    form = PageForm()
    if request.method == 'POST':
        form = PageForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/adm/article')
    template = 'adm-article.html'
    data = {
        'form': form,
    }

    return render_to_response(template, data, context_instance=RequestContext(request))


@login_required(login_url='/accounts/login/')
def delete_page(self, pk=None):
    """Delete an Artists"""
    Page.objects.get(pk=pk).delete()

    return HttpResponseRedirect('/adm/article')


@login_required(login_url='/accounts/login/')
def edit_page(self, pk=None):
    """Edit An Artist"""
    Page.objects.get(pk=pk)
    return HttpResponseRedirect('/adm/article')
