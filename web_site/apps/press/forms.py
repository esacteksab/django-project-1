from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
from crispy_forms.bootstrap import *
from web_site.apps.press.models import Article, Page


class ArticleForm(ModelForm):
    """A form to allow the creation of a new article"""
    class Meta:
        model = Article
        fields = ['title', 'publication', 'thumbnail', 'year']

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_id = 'add-article'
        self.helper.form_class = 'well form-inline'
        self.helper.form_action = ''
        self.helper.layout = Layout(
        Fieldset("Add a new Article",
            Field('title', css_class="input-large", placeholder="Article Title"),
            Field('publication', css_class="input-large", placeholder="Publication"),
            Field('year', css_class="input-large", placeholder="Year"),
            Field('thumbnail'),
        ),
        FormActions(
            Submit('submit', "Submit", css_class='btn'),
            Submit('cancel', "Cancel"),
        )
    )
        super(ArticleForm, self).__init__(*args, **kwargs)


class PageForm(ModelForm):
    """A form to allow the creation of a new page"""
    class Meta:
        model = Page
        fields = ['article', 'page', 'pgnumber']

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_id = 'add-artist'
        self.helper.form_class = 'well form-inline'
        self.helper.form_action = ''
        self.helper.layout = Layout(
        Fieldset("Add a new Page",
            Field('article', css_class="select", placeholder="Associated Article"),
            Field('pgnumber', placeholder="Page Number"),
            Field('page', placeholder="Artist Name"),
        ),
        FormActions(
            Submit('submit', "Submit", css_class='btn'),
            Submit('cancel', "Cancel"),
        )
    )
        super(PageForm, self).__init__(*args, **kwargs)
