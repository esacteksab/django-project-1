from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_thumbs.db.models import ImageWithThumbsField

from south.modelsinspector import add_introspection_rules

add_introspection_rules(
    [
        (
            (ImageWithThumbsField, ),
            [],
            {
                "verbose_name": ["verbose_name", {"default": None}],
                "name":         ["name",         {"default": None}],
                "width_field":  ["width_field",  {"default": None}],
                "height_field": ["height_field", {"default": None}],
                "sizes":        ["sizes",        {"default": None}],
            },
        ),
    ],
    ["^django_thumbs.db.models",])


class Article(models.Model):
    title = models.CharField(_("Title"), max_length=255, null=True, blank=True)
    publication = models.CharField(_("Publication"), max_length=255, null=True,
        blank=True, unique=True)
    year = models.PositiveSmallIntegerField(_("Year"), max_length=4, null=False, blank=False)
    thumbnail = ImageWithThumbsField(_("thumbnail"), upload_to="images/",
        blank=True, null=True, sizes=((250,250),))

    class Meta:
        ordering = ['-year']

    def __unicode__(self):
        return "'%s' - '%s'" % (self.title, self.publication)


class Page(models.Model):
    article = models.ForeignKey(Article, to_field='publication')
    page = ImageWithThumbsField(_("page"), upload_to="images/", blank=True, null=True,
        sizes=((250,250),))
    pgnumber = models.PositiveSmallIntegerField(null=False,
        blank=False)

    class Meta:
        ordering = ['pgnumber']

    def __unicode__(self):
        return '%s' '%s' % (self.article, self.page,)
