from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
from crispy_forms.bootstrap import *
from web_site.apps.artists.models import VisitingArtist


class ArtistForm(ModelForm):
    """A form to allow the creation of a new sponsor"""
    class Meta:
        model = VisitingArtist
        fields = ['name', 'shop', 'website']

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_id = 'add-artist'
        self.helper.form_class = 'well form-inline'
        self.helper.form_action = ''
        self.helper.layout = Layout(
        Fieldset("Add a new Artist",
            Field('name', css_class="input-large", placeholder="Artist Name"),
            Field('shop', css_class="input-large", placeholder="Artist Shop"),
            Field('website', css_class="input-large", placeholder="Artist Website"),
        ),
        FormActions(
            Submit('submit', "Submit", css_class='btn btn-large'),
        )
    )
        super(ArtistForm, self).__init__(*args, **kwargs)
