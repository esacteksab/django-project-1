from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.


class VisitingArtist(models.Model):
    name = models.CharField(_("name"), max_length=50, null=True, blank=True)
    shop = models.CharField(_("shop"), max_length=50, null=True, blank=True)
    website = models.URLField(_("website"), null=True, blank=True, verify_exists=False)

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return '%s' '%s' '%s' % (self.name, self.shop, self.website)
