from django.contrib import admin
from web_site.apps.artists.models import VisitingArtist


class VAAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,    {'fields': ['name']}),
        (None,    {'fields': ['shop']}),
        (None, {'fields': ['website']}),
    ]
    list_display = ('name', 'shop', 'website')

admin.site.register(VisitingArtist, VAAdmin)
