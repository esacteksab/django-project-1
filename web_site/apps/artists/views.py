# Create your views here.
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
from web_site.apps.artists.models import VisitingArtist
from web_site.apps.artists.forms import ArtistForm


@login_required(login_url='/accounts/login')
def add_artist(request):
    """Add a new Visiting Artist"""
    form = ArtistForm()
    if request.method == 'POST':
        form = ArtistForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/adm/artists')
    template = 'adm-artist.html'
    data = {
        'form': form,
    }

    return render_to_response(template, data, context_instance=RequestContext(request))


@login_required(login_url='/accounts/login/')
def delete_artist(self, pk=None):
    """Delete an Artists"""
    VisitingArtist.objects.get(pk=pk).delete()

    return HttpResponseRedirect('/adm/artists')


@login_required(login_url='/accounts/login/')
def edit_artist(self, pk=None):
    """Edit An Artist"""
    VisitingArtist.objects.get(pk=pk)
    return HttpResponseRedirect('/adm/sponsors')


def displayArtists(request):
    """Display all Aritsts"""
    artists = VisitingArtist.objects.all().order_by('name')
    return render_to_response('artists.html',
        {'artists': artists},
            context_instance=RequestContext(request))
