from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
from crispy_forms.bootstrap import *


class LoginForm(forms.Form):
    """A form to auth users"""

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_action = "{% url 'django.contrib.auth.views.login' %}"
        self.helper.layout = Layout(
            Field('username', css_class="input-large", placeholder="User Name"),
            Field('password', css_class="input-large", type="password"),
            ),
        FormActions(
            Submit('submit', "Submit", css_class='btn btn-large'),
        )
        super(LoginForm, self).__init__(*args, **kwargs)
