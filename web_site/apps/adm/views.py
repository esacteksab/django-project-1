# Create your views here.
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from web_site.apps.adm.forms import LoginForm
from web_site.apps.artists.models import VisitingArtist
from web_site.apps.sponsors.models import Sponsor


def user_login(request):
    """Log a User In"""
    form = LoginForm()
    if request.method == 'POST':
        form = LoginForm(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.active:
                login(request, user)
                return HttpResponseRedirect('/sponsors')
    template = 'login.html'
    data = {
        'form': form, 'username': username, 'password': password
    }

    return render_to_response(template, data, context_instance=RequestContext(request))


@login_required
def displayArtists(request):
    """Display all Aritsts"""
    artists = VisitingArtist.objects.all().order_by('name')
    return render_to_response('adm-artists.html',
        {'artists': artists},
            context_instance=RequestContext(request))

@login_required
def displaySponsors(request):
    """Display all Aritsts"""
    sponsors = Sponsor.objects.all()
    return render_to_response('adm-sponsors.html',
        {'sponsors': sponsors},
            context_instance=RequestContext(request))


def user_logout(request):
    """Log a user Out"""
    logout(request)
    return HttpResponseRedirect('/')


@login_required
def home(request):
    """ Display Homepage for Admin Site"""
    template = 'adm-index.html'
    return render_to_response(template, context_instance=RequestContext(request))
