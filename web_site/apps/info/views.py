import json
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.template.loader import render_to_string
from django.core.mail import send_mail
from web_site.apps.info.forms import ContactForm
from web_site.apps.artists.models import VisitingArtist
from web_site.apps.press.models import Article, Page
from django.views.decorators.cache import cache_page

def home(request):
    """Display Homepage Content"""
    artists = VisitingArtist.objects.all()
    return render_to_response('index.html',
        {'artists': artists},
            context_instance=RequestContext(request))
@cache_page(1 * 1)
def Contact(request):
    """ Contact Form for Home Page """
    artists = VisitingArtist.objects.all()
    articles = Article.objects.all().prefetch_related('page_set')
    data = {"html": "", "status": "success"}
    form = ContactForm()
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            message = "{name} / {email} said: ".format(
            name=form.cleaned_data.get('name'),
            email=form.cleaned_data.get('email'))
            message += "\n\n{0}".format(form.cleaned_data.get('message'))
            send_mail(
                subject=form.cleaned_data.get('subject'),
                message=message,
                from_email=form.cleaned_data.get('email'),
                recipient_list=['sfotattooconvention@gmail.com'],)
            data["html"] = render_to_string("_success.html", {},
             context_instance=RequestContext(request))
        else:
            data["status"] = "error"

            data["html"] = render_to_string("_form.html", {
            "form": form}, context_instance=RequestContext(request))

        return HttpResponse(json.dumps(data), mimetype="application/json")

    template = 'index.html'
    data = {
        'form': form,
        'artists': artists,
        'articles': articles,
    }

    return render_to_response(template, data, context_instance=RequestContext(request))
